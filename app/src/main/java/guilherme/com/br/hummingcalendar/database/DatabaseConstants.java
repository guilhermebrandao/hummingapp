package guilherme.com.br.hummingcalendar.database;

/**
 * Created by guilherme on 23/11/16.
 */

public interface DatabaseConstants {
    String TBLANIME         = "tbl_anime";

    String ID               = "id"; // Anime ID.
    String MAL_ID           = "mal_id"; //MyAnimeList ID.
    String SLUG             = "slug";
    String STATUS           = "status"; // Airing Status. Can be one of Not Yet Aired, Currently Airing, Finished Airing.
    String URL              = "url"; // Link to the anime page at Hummingbird's website.
    String TITLE            = "title"; // Main title.
    String ALTERNATETITLE   = "alternate_title"; //Alternative title. Can be empty.
    String EPISODECOUNT     = "episode_count"; //Total number of episodes.
    String EPISODELENGTH    = "episode_length"; // NAO TENHO CERTEZA SE INT. Length of an episode in minutes.
    String CONVERIMAGE      = "cover_image"; // URL to poster image.
    String SYNOPSIS         = "synopsis"; // Can be empty.
    String SHOWTYPE         = "show_type"; // Can be one of TV, Movie, OVA, ONA, Special, Music.
    String STARTEDAIRING    = "started_airing"; // Date in YYYY-MM-DD format.
    String FINISHEDAIRING   = "finished_airing"; //Date in YYYY-MM-DD format.
    String COMMUNITYRATING  = "community_rating"; //Can be a number between 0.0 and 5.0.
    String AGERATING        = "age_rating"; // Can be one of G, PG, PG13, R17+, R18+.

    StringBuilder CREATE_ANIME_TABLE = new StringBuilder()
        .append("create table " + TBLANIME +  " (")
            .append(ID + " integer primary key not null, ")
            .append(MAL_ID + " text, ")
            .append(SLUG + " text, ")
            .append(STATUS + " text not null, ")
            .append(URL + " text not null, ")
            .append(TITLE + " text not null, ")
            .append(ALTERNATETITLE + " text, ")
            .append(EPISODECOUNT + " integer not null, ")
            .append(EPISODELENGTH + " real not null, ")
            .append(CONVERIMAGE + " text not null, ")
            .append(SYNOPSIS + " text not null, ")
            .append(SHOWTYPE + " text not null, ")
            .append(STARTEDAIRING + " text not null, ")
            .append(FINISHEDAIRING + " text, ")
            .append(COMMUNITYRATING + " real, ")
            .append(AGERATING + " text")
            .append(");");

    StringBuilder ALL_FIELDS = new StringBuilder()
            .append(ID + ", ")
            .append(MAL_ID + ", ")
            .append(SLUG + ", ")
            .append(STATUS + ", ")
            .append(URL + ", ")
            .append(TITLE + ", ")
            .append(ALTERNATETITLE + ", ")
            .append(EPISODECOUNT + ", ")
            .append(EPISODELENGTH + ", ")
            .append(CONVERIMAGE + ", ")
            .append(SYNOPSIS + ", ")
            .append(SHOWTYPE + ", ")
            .append(STARTEDAIRING + ", ")
            .append(FINISHEDAIRING + ", ")
            .append(COMMUNITYRATING + ", ")
            .append(AGERATING);
}

