package guilherme.com.br.hummingcalendar.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by guilherme on 23/11/16.
 */

public class SqlHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "AnimeDB";
    public static final int DB_VERSION = 1;

    private static SqlHelper instance;
    private Context ctx;


    public SqlHelper(Context pContext) {
        super(pContext, DB_NAME, null, DB_VERSION);
        this.ctx = pContext;
    }

    public static synchronized SqlHelper getInstance(Context ctx) {
        if (instance == null) {
            instance = new SqlHelper(ctx);
        }
        return instance;
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseConstants.CREATE_ANIME_TABLE.toString());

    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
