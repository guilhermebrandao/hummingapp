package guilherme.com.br.hummingcalendar.http;

import java.util.List;

import guilherme.com.br.hummingcalendar.model.Anime;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by guilherme on 05/11/16.
 */

/**
 * Interface used by the Retrofit to create a Client Service.
 */
public interface HummingClient {

    /**
     * Sends a request to a HummingBird webserver and returns an Anime.
     *
     * @param id int Anime's ID.
     * @return Returns a Call of one Anime.
     */
    @GET("anime/{id}")
    Observable<Anime> getAnime(@Path("id") int id);

    /**
     * Sends a request to a HummingBird webserver and returns a list of Anime
     * based on its Title.
     *
     * @param query String Anime's Title.
     * @return Returns a Call of a Anime's List based on the title searched.
     */
    @GET("search/anime/")
    Observable<List<Anime>> getAnimes(@Query("query") String query);

}
