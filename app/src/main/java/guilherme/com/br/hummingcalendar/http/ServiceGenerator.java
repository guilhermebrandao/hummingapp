package guilherme.com.br.hummingcalendar.http;

/**
 * Created by guilherme on 05/11/16.
 */

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class to create a new REST client using Retrofit with a given API base url.
 * "API/HTTP client heart".
 */
public class ServiceGenerator {

    public ServiceGenerator(){

    }

    /**
     * HummingBird base API url.
     */
    public static final String API_BASE_URL = "http://hummingbird.me/api/v1/";

//    /**
//     * HttpClient.
//     */
//    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//
//    /**
//     * Retrofit RestAdapter Builder to create the new REST client.
//     */
//    private static Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
//            .baseUrl(API_BASE_URL)
//            .addConverterFactory(GsonConverterFactory.create());
//
//    /**
//     * @param serviceClass Defines the annotated class or interface for API requests.
//     * @param <S>          Class type parameter. It is used to specify that the output
//     *                     type class to be same as input class.
//     * @return Returns a service created for the input class.
//     */
//    public static <S> S createService(Class<S> serviceClass) {
//        Retrofit retrofit = retrofitBuilder.client(httpClient.build()).build();
//        return retrofit.create(serviceClass);
//    }

    public static HummingClient createHummingClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().
                addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();
        return retrofit.create(HummingClient.class);
    }
}
