package guilherme.com.br.hummingcalendar.model;

import org.parceler.Parcel;

import java.util.Date;
import java.util.List;

/**
 * Created by guilherme on 04/11/16.
 */

@Parcel
public class Anime {
    public int          id;
    public int          mal_id;         //MyAnimeList ID.
    public String       slug;
    public String       status;         // Airing Status. Can be one of Not Yet Aired, Currently Airing, Finished Airing.
    public String       url;            // Link to the anime page at Hummingbird's website.
    public String       title;          // Main title.
    public String       alternate_title;       //Alternative title. Can be empty.
    public int          episode_count;  //Total number of episodes.
    public int          episode_length;         // NAO TENHO CERTEZA SE INT. Length of an episode in minutes.
    public String       cover_image;          // URL to poster image.
    public String       synopsis;           // Can be empty.
    public String       show_type;          // Can be one of TV, Movie, OVA, ONA, Special, Music.
    public Date         started_airing;       // Date in YYYY-MM-DD format.
    public Date         finished_airing;     //Date in YYYY-MM-DD format.
    public double       community_rating;   //Can be a number between 0.0 and 5.0.
    public String       age_rating;         // Can be one of G, PG, PG13, R17+, R18+.
    public List<Genre>  genres;             //Array of genres.

    public Anime(){}

    public Anime(int id, int mal_id, String slug, String status, String url, String title,
                 String alternate_title, int episode_count, int episode_length, String cover_image,
                 String synopsis, String show_type, Date started_airing, Date finished_airing,
                 double community_rating, String age_rating, List<Genre> genres) {
        this.id = id;
        this.mal_id = mal_id;
        this.slug = slug;
        this.status = status;
        this.url = url;
        this.title = title;
        this.alternate_title = alternate_title;
        this.episode_count = episode_count;
        this.episode_length = episode_length;
        this.cover_image = cover_image;
        this.synopsis = synopsis;
        this.show_type = show_type;
        this.started_airing = started_airing;
        this.finished_airing = finished_airing;
        this.community_rating = community_rating;
        this.age_rating = age_rating;
        this.genres = genres;
    }


}
