package guilherme.com.br.hummingcalendar.model;

import org.parceler.Parcel;

/**
 * Created by guilherme on 04/11/16.
 */

@Parcel
public class Genre {

    public String name;

    public Genre(){}

    public Genre(String name) {
        this.name = name;
    }
}
