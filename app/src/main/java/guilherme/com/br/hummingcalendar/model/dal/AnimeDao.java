package guilherme.com.br.hummingcalendar.model.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import guilherme.com.br.hummingcalendar.database.DatabaseConstants;
import guilherme.com.br.hummingcalendar.database.SqlHelper;
import guilherme.com.br.hummingcalendar.model.Anime;
import guilherme.com.br.hummingcalendar.util.DateParse;

/**
 * Created by guilherme on 04/12/16.
 */

public class AnimeDao implements IDaoAnime {

    public static final String TAG = "SOME_DATABASE_SHIT";

    SqlHelper sqlHelper;


    public AnimeDao() {
    }

    public AnimeDao(Context context) {
        sqlHelper = SqlHelper.getInstance(context);
    }

    @Override
    public void insert(final Anime anime) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = sqlHelper.getWritableDatabase();


        values.put(DatabaseConstants.ID, anime.id);
        values.put(DatabaseConstants.MAL_ID, anime.mal_id);
        values.put(DatabaseConstants.SLUG, anime.slug);
        values.put(DatabaseConstants.STATUS, anime.status);
        values.put(DatabaseConstants.URL, anime.url);
        values.put(DatabaseConstants.TITLE, anime.title);
        values.put(DatabaseConstants.ALTERNATETITLE, anime.alternate_title);
        values.put(DatabaseConstants.EPISODECOUNT, anime.episode_count);
        values.put(DatabaseConstants.EPISODELENGTH, anime.episode_length);
        values.put(DatabaseConstants.CONVERIMAGE, anime.cover_image);
        values.put(DatabaseConstants.SYNOPSIS, anime.synopsis);
        values.put(DatabaseConstants.SHOWTYPE, anime.show_type);
        values.put(DatabaseConstants.STARTEDAIRING, String.valueOf(anime.started_airing));
        values.put(DatabaseConstants.FINISHEDAIRING, String.valueOf(anime.finished_airing));
        values.put(DatabaseConstants.COMMUNITYRATING, anime.community_rating);
        values.put(DatabaseConstants.AGERATING, anime.age_rating);

        try {
            db.insert(DatabaseConstants.TBLANIME, null, values);
            Log.wtf(TAG, "Anime  " + anime.title + " inserido com sucesso!");
        } catch (SQLiteException e) {
            Log.wtf(TAG, "Erro ao tentar inserir: \n " + e);
        } finally {
            db.close();
        }


    }

    @Override
    public final Anime get(final String title) {
        Anime anime = new Anime();
        SQLiteDatabase db = sqlHelper.getWritableDatabase();

        try {
            Cursor cursor = db.rawQuery("select " + DatabaseConstants.ALL_FIELDS.toString() +
                    " from " + DatabaseConstants.TBLANIME +
                    " where " + DatabaseConstants.TITLE + " = ? ", new String[]{title});

            if (cursor.moveToNext()) {
                anime = getFromCursor(cursor);
            }

            if (anime.title == null) {
                Log.wtf(TAG, "Busca Concluída: \n Anime não encontrado !");
            } else {
                Log.wtf(TAG, "Busca Concluída: \n Anime  " + anime.title + "!");
            }

        } catch (SQLiteException e) {
            Log.wtf(TAG, "Erro ao tentar buscar anime: \n " + e);
        } finally {
            db.close();
        }

        return anime;
    }

    @Override
    public List<Anime> getList() {
        List<Anime> animes = new ArrayList<Anime>();
        String sql = "select " + DatabaseConstants.ALL_FIELDS.toString() + " from " + DatabaseConstants.TBLANIME;

        SQLiteDatabase db = sqlHelper.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                animes.add(getFromCursor(cursor));
            }

            Log.wtf(TAG, "Total animes:  " + animes.size());
        } catch (SQLiteException e) {
            Log.wtf(TAG, "Erro ao tentar buscar animes: \n " + e);
        } finally {
            db.close();
        }
        return animes;
    }

    @Override
    public void remove(String title) {
        SQLiteDatabase db = sqlHelper.getWritableDatabase();

        try {
            db.delete(DatabaseConstants.TBLANIME, DatabaseConstants.TITLE + "=?", new String[]{title});

            Log.wtf(TAG, "Animes:  " + title + " deletado com sucesso!");
        } catch (SQLiteException e) {
            Log.wtf(TAG, "Erro ao tentar buscar animes: \n " + e);
        } finally {
            db.close();
        }
    }

    @Override
    public Anime getFromCursor(Cursor cursor) {
        Anime anime = new Anime();

        if (cursor != null) {
            anime.id = Integer.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseConstants.ID)));
            anime.mal_id = Integer.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseConstants.MAL_ID)));
            anime.slug = cursor.getString(cursor.getColumnIndex(DatabaseConstants.SLUG));
            anime.status = cursor.getString(cursor.getColumnIndex(DatabaseConstants.STATUS));
            anime.url = cursor.getString(cursor.getColumnIndex(DatabaseConstants.URL));
            anime.title = cursor.getString(cursor.getColumnIndex(DatabaseConstants.TITLE));
            anime.alternate_title = cursor.getString(cursor.getColumnIndex(DatabaseConstants.ALTERNATETITLE));
            anime.episode_count = Integer.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseConstants.EPISODECOUNT)));
            anime.episode_length = Integer.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseConstants.EPISODELENGTH)));
            anime.cover_image = cursor.getString(cursor.getColumnIndex(DatabaseConstants.CONVERIMAGE));
            anime.synopsis = cursor.getString(cursor.getColumnIndex(DatabaseConstants.SYNOPSIS));
            anime.show_type = cursor.getString(cursor.getColumnIndex(DatabaseConstants.SHOWTYPE));
            anime.started_airing = DateParse.formatarData(cursor.getString(cursor.getColumnIndex(DatabaseConstants.STARTEDAIRING)));
            anime.finished_airing = DateParse.formatarData(cursor.getString(cursor.getColumnIndex(DatabaseConstants.FINISHEDAIRING)));
            anime.community_rating = cursor.getDouble(cursor.getColumnIndex(DatabaseConstants.COMMUNITYRATING));
            anime.age_rating = cursor.getString(cursor.getColumnIndex(DatabaseConstants.AGERATING));
        }

        return anime;
    }
}