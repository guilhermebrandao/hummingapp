package guilherme.com.br.hummingcalendar.model.dal;

import android.database.Cursor;

import java.util.List;

import guilherme.com.br.hummingcalendar.model.Anime;

/**
 * Created by guilherme on 04/12/16.
 */

public interface IDaoAnime {

    /**
     * Inserts an anime into database.
     *
     * @param anime Favorite anime selected.
     */
    void insert(Anime anime);

    /**
     * Gets an anime by his title
     *
     * @param title String anime title.
     * @return returns an anime with all attributes fillded.
     */
    Anime get(String title);

    /**
     * @return Returns a list with all animes in the database.
     */
    List<Anime> getList();

    /**
     * Remove an anime from the database.
     *
     * @param title Anime's title.
     */
    void remove(String title);

    /**
     *
     * @param cursor
     * @return
     */
    Anime getFromCursor(Cursor cursor);
}
