package guilherme.com.br.hummingcalendar.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import guilherme.com.br.hummingcalendar.view.AnimeClick;
import guilherme.com.br.hummingcalendar.R;
import guilherme.com.br.hummingcalendar.databinding.AnimeItemBinding;
import guilherme.com.br.hummingcalendar.model.Anime;

/**
 * Created by guilherme on 05/11/16.
 */


public class AnimeAdapter extends RecyclerView.Adapter<AnimeAdapter.ViewHolder> {
    Context context;
    List<Anime> animes;
    Anime anime;
    AnimeItemBinding animeItemBinding;

    public AnimeAdapter(List<Anime> pAnimes, Context pContext) {
        this.animes = pAnimes;
        this.context = pContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        animeItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.anime_item,
                parent,
                false);

        final ViewHolder vh = new ViewHolder(animeItemBinding.getRoot());
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        anime = animes.get(position);
        holder.binding.setAnime(anime);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return this.animes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        AnimeItemBinding binding;

        public ViewHolder(final View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
            binding.cardView.setOnClickListener(View -> {
                if (context instanceof AnimeClick) {
                    ((AnimeClick) context).clickAnime(binding.getAnime());
                }
            });
        }
    }

}
