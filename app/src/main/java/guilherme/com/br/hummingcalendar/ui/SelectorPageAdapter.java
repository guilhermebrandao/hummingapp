package guilherme.com.br.hummingcalendar.ui;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import guilherme.com.br.hummingcalendar.view.fragment.FavoriteFragment;
import guilherme.com.br.hummingcalendar.view.fragment.HummingFragment;

/**
 * Created by guilherme on 04/12/16.
 */

public class SelectorPageAdapter extends FragmentPagerAdapter {

    HummingFragment hummingFragment;
    FavoriteFragment favoriteFragment;

    public SelectorPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (hummingFragment == null) {
                    hummingFragment = new HummingFragment();
                }
                return hummingFragment;
            case 1:
            default:
                if (favoriteFragment == null) {
                    favoriteFragment = new FavoriteFragment();
                }
                return favoriteFragment;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Lista";
            case 1:
            default:
                return "Favoritos";
        }

    }
}
