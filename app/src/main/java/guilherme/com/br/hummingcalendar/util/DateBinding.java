package guilherme.com.br.hummingcalendar.util;

import android.databinding.BindingAdapter;
import android.widget.TextView;

import java.util.Date;

/**
 * Created by guilherme on 05/11/16.
 */

public class DateBinding {
    @BindingAdapter({"android:text"})
    public static void setDateString (TextView textView, Date date){
        textView.setText(DateParse.formatarData(date));
    }
}
