package guilherme.com.br.hummingcalendar.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by guilherme on 05/11/16.
 */

public abstract class DateParse {


    public static final String DATE_PATTERN = "dd/MM/yyyy";

    /** O formatador de data. */
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat (DATE_PATTERN);

    /**
     * Transforma um objeto Date para uma string no formato "dd/MM/yyyy".
     *
     * @param data
     *            O objeto Date a ser formatado.
     * @return O texto contendo a data no formato especificado.
     */
    /**
     * Transforma um objeto Date para uma string no formato especificado.
     *
     * @param data
     *            O objeto Date a ser formatado.
     * @param formato
     *            O formato da data.
     * @return O texto contendo a data no formato especificado.
     */
    public static String formatarData(final Date data, final String formato) {
        SimpleDateFormat format;
        String dataFormatada;

        if (data != null) {
            format = new SimpleDateFormat(formato);
            dataFormatada = format.format(data);
        } else {
            dataFormatada = "";
        }
        return dataFormatada;
    }

    /**
     * Transforma um objeto Date para uma string no formato "dd/MM/yyyy".
     *
     * @param data
     *            O objeto Date a ser formatado.
     * @return O texto contendo a data no formato especificado.
     */
    public static String formatarData(final Date data) {
        return formatarData(data, DATE_PATTERN);
    }

    public static Date formatarData(final String dateString){
        try {
            Date date = DATE_FORMATTER.parse(dateString);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
