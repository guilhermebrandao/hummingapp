package guilherme.com.br.hummingcalendar.util;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by guilherme on 05/11/16.
 */

public class ImageBinding {
    @BindingAdapter({"android:src"})
    public static void setImgUrl(ImageView imageView, String url){
        Picasso.with(imageView.getContext())
                .load(url)
                .resize(72,96)
                .into(imageView);
    }
}
