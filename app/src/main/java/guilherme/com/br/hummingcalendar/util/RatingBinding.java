package guilherme.com.br.hummingcalendar.util;

import android.databinding.BindingAdapter;
import android.widget.RatingBar;

/**
 * Created by guilherme on 06/11/16.
 */

public class RatingBinding {
    @BindingAdapter({"android:rating"})
    public static void setRatingDouble(RatingBar ratingBar, double rating){
        ratingBar.setRating(Math.round(rating));
    }
}
