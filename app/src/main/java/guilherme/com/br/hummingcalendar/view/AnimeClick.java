package guilherme.com.br.hummingcalendar.view;

import guilherme.com.br.hummingcalendar.model.Anime;

/**
 * Created by guilherme on 03/12/16.
 */

public interface AnimeClick {
    void clickAnime (Anime anime);
}
