package guilherme.com.br.hummingcalendar.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.parceler.Parcels;

import guilherme.com.br.hummingcalendar.R;
import guilherme.com.br.hummingcalendar.databinding.ActivityDetailBinding;
import guilherme.com.br.hummingcalendar.model.Anime;
import guilherme.com.br.hummingcalendar.view.fragment.DetailFragment;

public class DetailActivity extends AppCompatActivity {

    ActivityDetailBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


//        setSupportActionBar(mBinding.toolbar);

        Anime anime = Parcels.unwrap(getIntent().getParcelableExtra("anime"));

        DetailFragment detailFragment = DetailFragment.newInstance(anime);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.detail_activity, detailFragment, "anime")
                .commit();

//        mBinding.getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }
}
