package guilherme.com.br.hummingcalendar.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.parceler.Parcels;

import guilherme.com.br.hummingcalendar.R;
import guilherme.com.br.hummingcalendar.databinding.ActivityHummingBinding;
import guilherme.com.br.hummingcalendar.model.Anime;
import guilherme.com.br.hummingcalendar.ui.SelectorPageAdapter;
import guilherme.com.br.hummingcalendar.view.fragment.DetailFragment;

/**
 * Main Class for the HummingCalendar activity.
 */
public class HummingActivity extends AppCompatActivity implements AnimeClick {

    /**
     * DataBinding for this activity.
     */
    ActivityHummingBinding mBinding;


    SelectorPageAdapter selectorPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_humming);
        setSupportActionBar(mBinding.toolbar);

        buildViewPager();
    }


    @Override
    public void clickAnime(final Anime anime) {
        if(getResources().getBoolean(R.bool.phone)) {
            Intent it = new Intent(this, DetailActivity.class);
            it.putExtra("anime", Parcels.wrap(anime));
            this.startActivity(it);
        } else {
            DetailFragment detailFragment = DetailFragment.newInstance(anime);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.detail_activity, detailFragment, "anime")
                    .commit();
        }
    }

    private void buildViewPager(){
        selectorPageAdapter = new SelectorPageAdapter(getSupportFragmentManager());
        mBinding.pager.setAdapter(selectorPageAdapter);
        mBinding.tabLayout.setupWithViewPager(mBinding.pager);
    }


}




