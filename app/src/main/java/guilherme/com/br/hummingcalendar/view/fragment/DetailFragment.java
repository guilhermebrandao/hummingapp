package guilherme.com.br.hummingcalendar.view.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.parceler.Parcels;

import guilherme.com.br.hummingcalendar.R;
import guilherme.com.br.hummingcalendar.database.DbEvent;
import guilherme.com.br.hummingcalendar.databinding.FragmentDetailBinding;
import guilherme.com.br.hummingcalendar.model.Anime;
import guilherme.com.br.hummingcalendar.model.dal.AnimeDao;
import guilherme.com.br.hummingcalendar.model.dal.IDaoAnime;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment {

    FragmentDetailBinding mBinding;
    IDaoAnime dao;

    public DetailFragment() {
        // Required empty public constructor
    }

    public static DetailFragment newInstance(Anime anime) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("anime", Parcels.wrap(anime));

        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(bundle);

        return detailFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false);

        Anime anime = Parcels.unwrap(getArguments().getParcelable("anime"));

        mBinding.content.setAnime(anime);

        mBinding.collapseToolbarLayout.setTitle(mBinding.content.getAnime().title);

        defineCollapseImage(anime.cover_image);

        dao = new AnimeDao(getActivity());
        Anime animeTemp = dao.get(anime.title);

        if (animeTemp.title == null) {
            changeFloatButton(false);
        } else {
            changeFloatButton(true);
        }

        mBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveOrRemoveFavorite(animeTemp, anime);
                Snackbar.make(view, "Anime " + anime.title + " adicionado aos favoritos", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        return mBinding.getRoot();
    }

    public void saveOrRemoveFavorite(final Anime pAnimeTemp, final Anime pAnime) {

        if (pAnimeTemp.title == null) {
            dao.insert(pAnime);
            changeFloatButton(true);
        } else {
            dao.remove(pAnimeTemp.title);
            changeFloatButton(false);
        }
        EventBus.getDefault().post(new DbEvent());
    }

    /**
     * @param urlImage String com a URL da imagem.
     */
    public void defineCollapseImage(String urlImage) {
        Picasso.with(getActivity())
                .load(urlImage)
                .into(mBinding.imageId);
    }

    public void changeFloatButton(boolean isFavorite) {
        int resource = isFavorite ? R.drawable.ic_star_black_24dp : R.drawable.ic_star_border_black_24dp;
        mBinding.fab.setImageResource(resource);
    }
}
