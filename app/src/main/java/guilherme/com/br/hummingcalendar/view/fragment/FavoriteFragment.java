package guilherme.com.br.hummingcalendar.view.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import guilherme.com.br.hummingcalendar.R;
import guilherme.com.br.hummingcalendar.database.DbEvent;
import guilherme.com.br.hummingcalendar.databinding.FragmentFavoriteBinding;
import guilherme.com.br.hummingcalendar.model.Anime;
import guilherme.com.br.hummingcalendar.model.dal.AnimeDao;
import guilherme.com.br.hummingcalendar.ui.AnimeAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteFragment extends Fragment {

    FragmentFavoriteBinding mFavoriteBinding;

    List<Anime> animes = new ArrayList<Anime>();

    public FavoriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mFavoriteBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite, container, false);

        EventBus.getDefault().register(this);
        updateUi();
        return mFavoriteBinding.getRoot();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    private void updateUi() {
        animes = new AnimeDao(getActivity()).getList();
        setManagerAndAdapter(animes);
    }

    /**
     * Define and use a LinearLayoutManager for the RecyclerView and sets the an Adapter to show
     * the list of Animes on user's screen.
     *
     * @param animes List of Animes to populate the RecyclerView.
     */
    void setManagerAndAdapter(List<Anime> animes) {

        // A RecyclerView needs to have a layout manager and an adapter to be instantiated.
        // A layout manager positions item views inside a RecyclerView and determines when
        // to reuse item views that are no longer visible to the user. (From guides.codepath.com)
        LinearLayoutManager manager = new LinearLayoutManager(this.getActivity());

        // Defines de Orientation to be Vertical.
        manager.setOrientation(LinearLayoutManager.VERTICAL);

        // Get the RecyclerView thru DataBinding and set the LayoutManager.
        mFavoriteBinding.favListAnimes.setLayoutManager(manager);

        // Instantiate a new AnimeAdapter with a given List<Anime>.
        AnimeAdapter animeAdapter = new AnimeAdapter(animes, getActivity());

        // Thru DataBinding, set the Adapter to be the AnimeAdapter
        mFavoriteBinding.favListAnimes.setAdapter(animeAdapter);

        animeAdapter.notifyDataSetChanged();

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DbEvent event) {
        updateUi();
    }

}
