package guilherme.com.br.hummingcalendar.view.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import guilherme.com.br.hummingcalendar.R;
import guilherme.com.br.hummingcalendar.databinding.FragmentAnimeListBinding;
import guilherme.com.br.hummingcalendar.http.HummingClient;
import guilherme.com.br.hummingcalendar.http.ServiceGenerator;
import guilherme.com.br.hummingcalendar.model.Anime;
import guilherme.com.br.hummingcalendar.ui.AnimeAdapter;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 */
public class HummingFragment extends Fragment implements SearchView.OnQueryTextListener {

    /**
     * TAG for logging.
     */
    public static final String TAG = "GUILHERME";

    FragmentAnimeListBinding mBinding;

    Subscription subscription;

    public HummingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_anime_list, container, false);

        EventBus.getDefault().register(this);

        return mBinding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        callAnimesRx(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void callAnimesRx(String anime) {
        final HummingClient service = ServiceGenerator.createHummingClient();
        subscription = service.getAnimes(anime)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        animes -> EventBus.getDefault().postSticky(animes),
                        Throwable::printStackTrace,
                        () -> subscription.unsubscribe());
    }


    /**
     * Define and use a LinearLayoutManager for the RecyclerView and sets the an Adapter to show
     * the list of Animes on user's screen.
     *
     * @param animes List of Animes to populate the RecyclerView.
     */
    void setManagerAndAdapter(List<Anime> animes) {

        // A RecyclerView needs to have a layout manager and an adapter to be instantiated.
        // A layout manager positions item views inside a RecyclerView and determines when
        // to reuse item views that are no longer visible to the user. (From guides.codepath.com)
        LinearLayoutManager manager = new LinearLayoutManager(this.getActivity());

        // Defines de Orientation to be Vertical.
        manager.setOrientation(LinearLayoutManager.VERTICAL);

        // Get the RecyclerView thru DataBinding and set the LayoutManager.
        mBinding.listAnimes.setLayoutManager(manager);

        // Instantiate a new AnimeAdapter with a given List<Anime>.
        AnimeAdapter animeAdapter = new AnimeAdapter(animes, getActivity());

        // Thru DataBinding, set the Adapter to be the AnimeAdapter
        mBinding.listAnimes.setAdapter(animeAdapter);

        animeAdapter.notifyDataSetChanged();

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(List<Anime> event) {
        setManagerAndAdapter(event);
    }

}
